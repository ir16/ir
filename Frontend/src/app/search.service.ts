import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DT } from './model/Document.model';


@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }


  public query(dataset: boolean, q: string) {
    return this.http.post<DT[]>("http://127.0.0.1:5000/query",
      JSON.stringify({
        "name_data_set": dataset,
        "query": q
      },
      )
      ,{
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Accept':'*/*'
        }
        // responseType: 'text'
      },
      

    )
  }


  public correct(q: string) {
    return this.http.post<string>("http://127.0.0.1:5000/correct",
      JSON.stringify({
        "query": q
      },
      )
      ,{
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Accept':'*/*'
        }
        // responseType: 'text'
      },
      

    )
  }


  public sugget(q: string) {
    return this.http.post<string[]>("http://127.0.0.1:5000/sugget",
      JSON.stringify({
        "sentence": q
      },
      )
      ,{
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Accept':'*/*'
        }
        // responseType: 'text'
      },
      

    )
  }
}

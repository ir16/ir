import { Component, Input, OnInit } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { DT } from 'src/app/model/Document.model';


@Component({
  selector: 'app-view-item',
  templateUrl: './view-item.component.html',
  styleUrls: ['./view-item.component.css']
})
export class ViewItemComponent implements OnInit {

  @Input('data') data: DT |any;
  formModal: any;
  id: number = -1;
  
  constructor(public ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit(): void {
    this.id = this.data.id
  }

  getData(){
    console.log(this.data.title);
    
    return this.data
  }

  show(){
    // if(this.data.id == this.id)
    this.ngxSmartModalService.getModal(this.data.title).open()
  }


}

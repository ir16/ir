import { Component, Input, OnInit } from '@angular/core';
import { DT } from '../model/Document.model';

@Component({
  selector: 'app-view-results',
  templateUrl: './view-results.component.html',
  styleUrls: ['./view-results.component.css']
})
export class ViewResultsComponent implements OnInit {

  @Input('data') data: DT[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}

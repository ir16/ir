import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DT } from '../model/Document.model';
import { SearchService } from '../search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  @Input('ds') ds : boolean = false;
  @Output('getData') getData : EventEmitter<DT[]> =   new EventEmitter();

  constructor(
    private s: SearchService
  ) { }

  ngOnInit(): void {
  }

  submit(v:any){
    console.log(this.ds)
    this.s.query(this.ds,v.value).subscribe((x:DT[]) =>  {
      console.log("r")
      console.log(x)
      this.getData.emit(x)
    }  )
  }

  getUserList() {
   return 'test'
}


  keyword = 'name';
  data:string[] = [];

  d = '';

  
  userList1: string[] = [
    
  ]

  selectEvent(item:any) {
    // do something with selected item
  }

  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    console.log(val);
    this.s.correct(val).subscribe( (x:string) => {
      this.data = []
      // this.data = x
      this.data.push(x) 
    }
    )
    console.log(this.data)
  }

  filed: any;
  change(val:any){
    console.log(val.value);
    this.s.correct(val.value).subscribe( (x:string) => {
      this.d = x
    }
    )
    console.log(this.data)

    this.s.sugget(val.value).subscribe( (x) => {
      this.userList1 = []
      // this.data = x
      this.userList1 = x
    } )
  }

  changeInput(){
    // console.log("good")
    this.filed = this.d;
  }
  
  onFocused(e:any){
    // do something when input is focused
  }

}

import { Component } from '@angular/core';
import { DT } from './model/Document.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';
  data: DT[] = [];

  datasetChoose : boolean = false; 

  onItemChange(v:any){
    console.log(v.value);
    if(v.value == "CISI") this.datasetChoose = false
    else if (v.value == "cacm") this.datasetChoose = true
  }

  getDataFromApp(v:any){
    console.log("srere")
    console.log(v)
    this.data = v
  }
}

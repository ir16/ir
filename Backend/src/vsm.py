import sys
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd
import joblib
import pickle
from nlp import NLP

nlp = NLP()


class VSM:

    vectorizerX = TfidfVectorizer()
    doc_vector = any

    def __init__(self) -> None:
        # cour = nlp.pre_process_doc()
        # self.build_index(cour)
        print("")
        # print(self.doc_vector)        


    def build_index(self, cour ):
       
        self.vectorizerX.fit(cour)
        self.doc_vector = self.vectorizerX.transform(cour)

        # df1 = pd.DataFrame(self.doc_vector.toarray(), columns=self.vectorizerX.get_feature_names())

        if NLP.datasetNum == False :
            #Save 
            joblib.dump(self.doc_vector, "index_CISI.pkl")
            #Save 
            joblib.dump(self.vectorizerX, "vectorizer_CISI.pkl")
        elif NLP.datasetNum == True:
            #Save 
            joblib.dump(self.doc_vector, "index_cacm.pkl")
            #Save 
            joblib.dump(self.vectorizerX, "vectorizer_cacm.pkl")
        
            
        # nlp.save_result_on_file(df1,"index")
        # print(df1)
        return self.doc_vector

    def query_create_vector(self, q):
        self.load_index()
        query_vector = self.vectorizerX.transform([q])
        return query_vector
    
    def match(self,query_vector):
        cosineSimilarities = cosine_similarity(self.doc_vector,query_vector).flatten()
        # print(cosineSimilarities[0],file=sys.stderr)
        # print(cosineSimilarities[1],file=sys.stderr)
        # print(cosineSimilarities[4],file=sys.stderr)
        # print(cosineSimilarities[1213],file=sys.stderr)
        # print(cosineSimilarities[1182],file=sys.stderr)
        # print(cosineSimilarities[1344],file=sys.stderr)
        related_docs_indices = cosineSimilarities.argsort()[:-11:-1]
        print(related_docs_indices,file=sys.stderr)
        return related_docs_indices


    

    # this method for test
    def get_all_retrived(self,query_vector):
        cosineSimilarities = cosine_similarity(self.doc_vector,query_vector).flatten()
        # print(cosineSimilarities[59],file=sys.stderr)
        # print(cosineSimilarities[33],file=sys.stderr)
        # arr = np.array(cosineSimilarities)
        # arr = arr[arr != 0.0]
        # arr = list(arr)
        # arr = arr.sort()
        
        # arr = np.delete(cosineSimilarities, np.argwhere(cosineSimilarities == 0.0))
        # # cosineSimilarities = cosineSimilarities.remove(0.0)
        # # arr = arr.argsort()
        # print(arr[59],file=sys.stderr)
        # print(arr[33],file=sys.stderr)
        related_docs_indices = cosineSimilarities.argsort()[:-31:-1]
        return  related_docs_indices


    def load_index(self):
        # print(datasetNum, file=sys.stderr)
        # after tranineg --> after level offline and result saved
        if NLP.datasetNum == False :
            self.doc_vector  = joblib.load("index_CISI.pkl")
            self.vectorizerX = joblib.load("vectorizer_CISI.pkl")
        elif NLP.datasetNum == True:
            self.doc_vector  = joblib.load("index_cacm.pkl")
            self.vectorizerX = joblib.load("vectorizer_cacm.pkl")
            
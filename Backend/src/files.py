

import json
from xml.dom.minidom import Document
from flask import jsonify


nameDirectoryFolder = "\\files_data_set\\"


def readFile(url:str ,fileName: str):
    f = open(url + '\\' + fileName, "r")
    return f.read()



def spiltDataSetToFiles(data, url:str ):
    courps = []
    data = data.split(".I ")
    for i in range(1, len(data)):
        file = open(url + nameDirectoryFolder +str(i)+".txt","w+")
        data[i] = data[i][0: data[i].index('.X')]
        file.write(data[i])
        courps.append(data[i])
    return courps


def getFiles(arrayOfindex,url, filChoose:bool):
    data = []

    f = open(url + str(arrayOfindex[1]+1) + ".txt").read()


    if (filChoose == False):
        for i in arrayOfindex:
            # print(i)
            f = open(url + str(i+1) + ".txt").read()
            f = f.replace('\n', '')
            id   =    f[0:f.index('.T')]
            title   = f[f.index('.T')+2: f.index('.A')]
            author  = f[f.index('.A')+2: f.index('.W')]
            subject = f[f.index('.W')+2:]
            document = Document(int(id),title,subject,author,"")
            data.append(document.toJSON())
            # data.append()
    
    elif(filChoose == True):
        for i in arrayOfindex:
            # print(i)
            f = open(url + str(i+1) + ".txt").read()
            f = f.replace('\n', '')
            id   =    f[0:f.index('.T')]

            author = ""
            isW = False 
            if(f.find('.W')!= -1):
                # print("number cont w -->" ,str(i+1)) 
                isW = True

            if(isW == True):
                title   = f[f.index('.T')+2: f.index('.W')]
                if(f.find('.A')!=-1):
                    subject = f[f.index('.W')+2: f.index('.A')]
                else:
                    subject = f[f.index('.W')+2: f.index('.B')]
                
            else:
                print("number not fi" ,str(i+1) )
                title   = f[f.index('.T')+2: f.index('.B')]
                if(f.find(".K")!=-1):
                    subject = f[f.index('.K')+2: f.index('.N')]
                    if(f.find(".A")!=-1):
                        author  = f[f.index('.A')+2: f.index('.K')]
                    else:
                        author = ""
                subject = ""

            if(author == "" and f.find('.A')!=-1):
                author  = f[f.index('.A')+2: f.index('.N')]
            other   = f[f.index('.N')+2: ]
            
            document = Document(int(id),title,subject,author,other)
            data.append(document.toJSON())
            # print(data)
    
    # data = convertToJson(data)
    return data
        
    # title = f[f.index('.T'): f.index('.A')]
    # author =  '.W' in f == f[f.index('.A'): f.index('.W')] else 
            
    # subject = f[f.index('.T'): f.index('.A')]

    # print("Title is --> "+ str(title))
    # print("author is --> "+ str(author))
    # print("subject is --> "+ str(subject))

    # for i in arrayOfindex:
    #     print(i)
    #     f = open(url + str(i+1) + ".txt").read()
    #     data.append(f)
    #     # data.append()
    # return data




class Document:
    id: int
    title: str
    subject: str
    author: str
    other: str 

    def __init__(self,id, title, subject,author, other):
        self.id = id
        self.title = title
        self.subject = subject
        self.author = author
        self.other = other
    
    def toJSON(self):
        x = {
            "id": int(self.id),
            "title": self.title,
            "subject": self.subject,
            "author": self.author,
            "other": self.other,
        }
        # print(x)
        return x




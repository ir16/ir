from numpy import number

from controller import Controller
from vsm import VSM

number_real_query = 84


def get_MRR():
    
    print("")
    query_not_result = [ 36 , 38 , 40 , 47 , 48 , 51 , 53 , 59 , 60 , 63 , 64 , 68 ,
        70 , 72, 73, 74, 75, 77, 78, 80, 83, 85, 86, 87, 88, 89, 91, 93, 94, 
        103, 105, 106, 107, 108, 110, 112
        ]
    avg_rr = 0.0
    for i in range(1,113):
        if(i in query_not_result): 
            continue
        query = get_id_and_query_s(i)
        res = get_RR(i,query)
        avg_rr += res
        # print("res-->" + str(i) + "|" + str(res))

    number_all_q = number_real_query - len(query_not_result)


    mrr = avg_rr/number_all_q
    print ("MRR is --> : "+str(mrr))


def get_RR(id_query:number,query:str):
    all_relevent = get_id_doc_relevent(id_query)
    desired_array = [int(numeric_string) for numeric_string in all_relevent]

    contr = Controller()
    # q = "What is information science?  Give definitions where possible."
    query_after_process = contr.query_pre_process(query)

    vsm = VSM()
    query_vector = vsm.query_create_vector(query_after_process)
    res = vsm.match(query_vector)


    number_relevent = 0
    for i in range(len(res)+1):
        if(i == len(res)):
            if(number_relevent == 10):
                number_relevent = 0
            break
        if res[i]+1 not in desired_array:
            number_relevent += 1
            print(res[i])
        elif res[i]+1 in desired_array:
            number_relevent += 1
            break

    res = 0
    if(number_relevent != 0):
        res = 1/number_relevent
    return res

    




def get_MAP():
    print("test")
    query_not_result = [ 36 , 38 , 40 , 47 , 48 , 51 , 53 , 59 , 60 , 63 , 64 , 68 ,
        70 , 72, 73, 74, 75, 77, 78, 80, 83, 85, 86, 87, 88, 89, 91, 93, 94, 
        103, 105, 106, 107, 108, 110, 112
        ]
    # print (len(query_not_result))
    avg_ap = 0.0
    for i in range(1,113):
        if(i in query_not_result): 
            continue
        query = get_id_and_query_s(i)
        res = get_ap(i,query)
        avg_ap += res
        # print("res-->" + str(i) + "|" + str(res))
    number_all_q = number_real_query - len(query_not_result)

    map = avg_ap/number_all_q
    print("")
    print("")
    print ("MAP is --> :" + str(map))

def get_ap(id_query:number,query:str):
    all_relevent = get_id_doc_relevent(id_query)
    desired_array = [int(numeric_string) for numeric_string in all_relevent]

    contr = Controller()
    # q = "What is information science?  Give definitions where possible."
    query_after_process = contr.query_pre_process(query)

    vsm = VSM()
    query_vector = vsm.query_create_vector(query_after_process)
    res = vsm.match(query_vector)

    ap = 0.0
    #cycle --> p@cycle
    for cycle in range(len(res)+1):
        number_relevent = 0
        for i in range(cycle):
            if res[i]+1 in desired_array:
                number_relevent += 1
        
        # print(cycle)
        # print(number_relevent)
        # print("---")
        if(cycle!=0):
            r = number_relevent/cycle
            ap += r
    
    res = ap/10
    return res
    

def relevent_retrived(id_query:number,q:str):
    all_relevent = get_id_doc_relevent(id_query)
    desired_array = [int(numeric_string) for numeric_string in all_relevent]
    # print(desired_array)
    print("all relevent:" + str(len(desired_array)))

    contr = Controller()
    # q = "Current online library network technology is described, including the physical and functional aspects of networks.  Three types of networks are distinguished:  search service (e.g., SDC, Lockheed), customized service that provide bibliographic files (e.g., OCLC, Inc., RLIN), and service center (e.g., NELINET, INCOLSA).  It is predicted that as technology evolves more services will be provided outside the library directly to the user through his home or office."
    query_after_process = contr.query_pre_process(q)

    vsm = VSM()
    query_vector = vsm.query_create_vector(query_after_process)
    res = vsm.get_all_retrived(query_vector)

    print("All Retrived--:" + str(len(res)))

    retrived = []
    not_retrived = []
    for value in desired_array:
        for i in range(len(res)):
            if res[i] != 0.0:
                if value == res[i]+1:
                    retrived.append(value)
                    break            
        
    

    print("--")
    print("retrived :" + str(len(retrived)))
    print("--")
    print("not retrived: " +  str(len(desired_array) - len(retrived)))


def relevent_retrived_r(id_query:number):
    all_relevent = get_id_doc_relevent(id_query)
    desired_array = [int(numeric_string) for numeric_string in all_relevent]
    # print(desired_array)
    print("all relevent:" + str(len(desired_array)))

    contr = Controller()
    # q = "What is information science?  Give definitions where possible."
    q = "The frequency characteristics of terms in the documents of a collection have been used as indicators of term importance for content analysis and indexing purposes.  In particular, very rare or very frequent terms are normally believed to be less effective than medium-frequency terms.  Recently automatic indexing theories have been devised that use not only the term frequency characteristics but also the relevance properties of the terms. The major term-weighting theories are first briefly reviewed.  The term precision and term utility weights that are based on the occurrence characteristics of the terms in the relevant, as opposed to the nonrelevant, documents of a collection are then introduced.  Methods are suggested for estimating the relevance properties of the terms based on their overall occurrence characteristics in the collection.  Finally, experimental evaluation results are shown comparing the weighting systems using the term relevance properties with the more conventional frequency-based methodologies."
    query_after_process = contr.query_pre_process(q)

    vsm = VSM()
    query_vector = vsm.query_create_vector(query_after_process)
    res = vsm.match(query_vector)
    print("All Retrived--:" + str(len(res)))

    retrived = []
    not_retrived = []
    for value in desired_array:
        for i in res:
                if value == i+1:
                    retrived.append(value)
                    break            
        
    print("--")
    print("retrived :" + str(len(retrived)))
    print("--")
    print("not retrived: " +  str(len(desired_array) - len(retrived)))

    # print("--")
    # print(len(retrived))
    # print("--")
    # print(len(desired_array) - len(retrived))



def get_id_and_query_s(id:number):
    with open('D:\\IT\\5th\\Subject\\2\\IR\\P\\final projet\\IR_Project_datasets\\CISI\\CISI.QRY') as f:
        lines = ""
        for l in f.readlines():
            lines += "\n" + l.strip() if l.startswith(".") else " " + l.strip()
        lines = lines.lstrip("\n").split("\n")
    
    qry_set = {}
    qry_id = ""
    for l in lines:
        if l.startswith(".I"):
            qry_id = l.split(" ")[1].strip()
        elif l.startswith(".W"):
            qry_set[qry_id] = l.strip()[3:]
            qry_id = ""
    
    # Print something to see the dictionary structure, etc.
    # print(f"Number of queries = {len(qry_set)}" + ".\n")
    # print(qry_set[str(id)]) # note that the dictionary indexes are strings, not numbers. 
    return qry_set[str(id)]


      


def get_id_doc_relevent(id_query: number):
    # f = open('D:\\IT\\5th\\Subject\\2\\IR\\P\\final projet\\IR_Project_datasets\\CISI\\CISI.REL','r')
    rel_set = {}
    with open('D:\\IT\\5th\\Subject\\2\\IR\\P\\final projet\\IR_Project_datasets\\CISI\\CISI.REL') as f:
        for l in f.readlines():
            qry_id = l.lstrip(" ").strip("\n").split("\t")[0].split(" ")[0]
            doc_id = l.lstrip(" ").strip("\n").split("\t")[0].split(" ")[-1]
            if qry_id in rel_set:
                rel_set[qry_id].append(doc_id)
            else:
                rel_set[qry_id] = []
                rel_set[qry_id].append(doc_id)
            # if qry_id == "7":
            #     print(l.strip("\n"))
        
    # Print something to see the dictionary structure, etc.
    # print(f"\nNumber of mappings = {len(rel_set)}" + ".\n")
    return rel_set[str(id_query)] # note that the dictionary indexes are strings, not numbers. 


## p@10
# relevent_retrived(102,' The frequency characteristics of terms in the documents of a collection have been used as indicators of term importance for content analysis and indexing purposes.  In particular, very rare or very frequent terms are normally believed to be less effective than medium-frequency terms.  Recently automatic indexing theories have been devised that use not only the term frequency characteristics but also the relevance properties of the terms. The major term-weighting theories are first briefly reviewed.  The term precision and term utility weights that are based on the occurrence characteristics of the terms in the relevant, as opposed to the nonrelevant, documents of a collection are then introduced.  Methods are suggested for estimating the relevance properties of the terms based on their overall occurrence characteristics in the collection.  Finally, experimental evaluation results are shown comparing the weighting systems using the term relevance properties with the more conventional frequency-based methodologies.')
# relevent_retrived(3,'What is information science?  Give definitions where possible.')
relevent_retrived_r(102)


## ap
# get_ap(3)

## map
# get_MAP()

## mrr
# get_MRR()


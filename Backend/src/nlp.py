import json
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
import re
import enchant
import nltk
import files as files
from textblob import TextBlob



url = "D:\\IT\\5th\\Subject\\2\\IR\\P\\final projet\\project\\output\\"
urlirr = "D:\\IT\\5th\\Subject\\2\\IR\\P\\final projet\\project\\input\\Irregular_verbs.txt"
urlStopWords = "D:\\IT\\5th\\Subject\\2\\IR\\P\\final projet\\IR_Project_datasets\\cacm\\common_words.txt"


class NLP:

    datasetNum = False

    def save_result_on_file(self,w, nameFile:str):
        file = open( url + "result_"+ nameFile +".txt","w+")
        file.write(str(w))

    def token(self, data: str):
        w = word_tokenize(data)
        self.save_result_on_file(w,"token")
        return w
    
    def Stremaing(self, data: str):
        token_all_word = self.token(data)

        ps = PorterStemmer()
        for i in range(len(token_all_word)):
            token_all_word[i] = ps.stem(token_all_word[i])

        self.save_result_on_file(token_all_word,"streaming")
        return token_all_word

    def detect_irregular(self,data):
        z = []
        f = open( urlirr,"r")
        all_irr = f.read().replace('\n', '').split(" ")
        iir_in_our_ds = [t for t in all_irr if t  in data]
        # print(iir_in_our_ds)
        self.save_result_on_file(iir_in_our_ds,"Irregular_verbs")


    def detect_date(self, data):
        x = re.findall(r'\d{1,2} (?:Jan|Feb|Mar|Apr|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \d{2,4}' ,data)
        y = re.findall(r'\d{1,2}[/-]\d{1,2}[/-]\d{2,4}' ,data)
        z = re.findall("(?:January|February|March|April|May|June|July|August|September|October|November|December) [a-z]* \d{2,4}",data)
        z1 = re.findall("(?:January|February|March|April|May|June|July|August|September|October|November|December) \d{2,4}",data)
        res = x+y+z+z1
        # print (res)
        self.save_result_on_file(res,"date")


    def suggest_correct(self):
        d = enchant.Dict("en_US") 
        print("start")
        word = input("Enter word: ")
        d.check(word) 
        out = d.suggest(word)
        self.save_result_on_file(out,"suggest_correct") 

    def lemmatize(self, data):
        lemmatizer = WordNetLemmatizer()
        for i in range(len(data)):
            data[i] = lemmatizer.lemmatize(data[i],pos="n")
        print (data)
        self.save_result_on_file(data,"Lemmatize") 



    # this function returns a list of tokenized and stemmed words of any text
    def get_tokenized_list(self, doc_text):
        tokens = nltk.word_tokenize(doc_text)
        return tokens

    # This function will performing stemming on tokenized words
    def word_stemmer(self, token_list):
        p = nltk.stem.PorterStemmer()
        stemmed = []
        for words in token_list:
            stemmed.append(p.stem(words))
        return stemmed

    # Function to remove stopwords from tokenized word list
    def remove_stopwords(self, doc_text):

        f = open(urlStopWords, "r")
        stop_words = f.read()
        # print(stop_words[0])

        cleaned_text = []
        for words in doc_text:
            if words not in stop_words:
             cleaned_text.append(words)
        return cleaned_text

    # limit
    def get_limit_list(self, doc_text):
        lemmatizer = WordNetLemmatizer()
        # print("---------")
        # print(doc_text)
        l = []
        for words in doc_text:
            l.append(lemmatizer.lemmatize(words,pos="n"))
        # print(l)
        return l




    def get_coorect_sen(self,s: str):
        sentence = TextBlob(s)
        result = sentence.correct()
        return str(result)




    # dataset CISI --> 0 , cacm --> 1  
    def pre_process_doc(self):
        urlDataset1 = "D:\\IT\\5th\\Subject\\2\\IR\\P\\final projet\\IR_Project_datasets\\CISI"
        filenameDS1 = "CISI.ALL"

        urlDataset2 = "D:\\IT\\5th\\Subject\\2\\IR\\P\\final projet\\IR_Project_datasets\\cacm"
        filenameDS2 = "cacm.ALL"

        if NLP.datasetNum == False :
            d1 = files.readFile(urlDataset1, filenameDS1)
            c = files.spiltDataSetToFiles(d1,urlDataset1)
        elif NLP.datasetNum == True :
            d2 = files.readFile(urlDataset2,filenameDS2)
            c = files.spiltDataSetToFiles(d2,urlDataset2)
        
        # # Working with file
        # d1 = files.readFile(urlDataset1, filenameDS1)
        # d2 = files.readFile(urlDataset2,filenameDS2)

        # # convert from one doc to multi docs
        # c = files.spiltDataSetToFiles(d1,urlDataset1)
        # c = files.spiltDataSetToFiles(d2,urlDataset2)

        # print(c)

        # d = d1 + d2


        cleaned_corpus = []
        for doc in c:
            tokens = self.get_tokenized_list(doc)
            doc_text = self.remove_stopwords(tokens)
            doc_text  = self.word_stemmer(doc_text)
            doc_text  = self.get_limit_list(doc_text)
            doc_text = ' '.join(doc_text)
            cleaned_corpus.append(doc_text)
        return cleaned_corpus

        
from flask import Flask, Response, jsonify,request
from flask_cors import CORS
from controller import Controller

from nlp import NLP
import pandas as pd

app = Flask(__name__)
CORS(app)
# app.config['CORS_HEADERS'] = 'Content-Type'
nlp = NLP()

@app.route('/')
# @cross_origin()
def init():
    return jsonify({
        'result': "The server is work"
    })


#http://127.0.0.1:5000/correct
@app.route('/correct', methods = ['POST'])
def c():
    queryRequset = { 
        'query': request.json['query']
         }
    
    controller = Controller()
    res = controller.correctQuery(queryRequset['query'])
    print(res)
    return jsonify(
        # 'result': pd.Series(res).to_json(orient='values'),
        # 'res': res
        res
    
    )


#http://127.0.0.1:5000/sugget
@app.route('/sugget', methods = ['POST'])
def sugg():
    queryRequset = { 
        'sentence': request.json['sentence']
         }
    
    controller = Controller()
    res = controller.sugget(queryRequset['sentence'])
    print(res)
    return jsonify(
        # 'result': pd.Series(res).to_json(orient='values'),
        # 'res': res
        res
    )



#http://127.0.0.1:5000/query
@app.route('/query', methods = ['POST'])
def query():


    queryRequset = { 
        'name_data_set': request.json['name_data_set'],
        'query': request.json['query']
         }
   

    controller = Controller()
    controller.setDataSet(bool(queryRequset['name_data_set']))
    res = controller.proceccing_query(queryRequset['query'])
    
    # print(queryRequset['query'], file=sys.stderr)

    # return Response(res, mimetype='application/json')
    return jsonify(
        # 'result': pd.Series(res).to_json(orient='values'),
        # 'res': res
        res
    
    )

@app.route('/all-query', methods = ['POST'])
def all_query():


    queryRequset = { 
        'name_data_set': request.json['name_data_set'],
        'query': request.json['query']
         }
   

    controller = Controller()
    controller.setDataSet(bool(queryRequset['name_data_set']))
    res = controller.get_all_index_retrived(queryRequset['query'])
    res = res[res != 0.0]
    # print(queryRequset['query'], file=sys.stderr)

    # return Response(res, mimetype='application/json')
    return jsonify(
      { 
           'count': len(res) 
        #    'result': pd.Series(res).to_json(orient='values'),
        # 'res': res
        # res
    }
    )







if __name__ == '__main__':
    app.run(debug=True)
import sys
from nlp import NLP
from vsm import VSM
import files
import joblib
from suggestion import Suggestion

nlp = NLP()
vsm = VSM()
sugget = Suggestion()

class Controller:


    def setDataSet(self, num:bool):
        print("num is "+ str(num),file=sys.stderr)
        NLP.datasetNum = num
    
    def correctQuery(self,q: str):
        res = nlp.get_coorect_sen(q)
        return res

    def sugget(self, q:str):
        # sugget.load_model()
        res = sugget.suggest(q)
        return res

    def get_all_index_retrived(self, q: str):
        query_after_process = self.query_pre_process(q)
        query_vector = vsm.query_create_vector(query_after_process)

        res = vsm.get_all_retrived(query_vector)
        return res

    def proceccing_query(self, q: str):
        query_after_process = self.query_pre_process(q)
        query_vector = vsm.query_create_vector(query_after_process)

        res = vsm.match(query_vector)
        
        url: str
        if(NLP.datasetNum == False):
            url = "D:\\IT\\5th\\Subject\\2\\IR\\P\\final projet\\IR_Project_datasets\\CISI\\files_data_set\\"
        elif(NLP.datasetNum == True):
            url = "D:\\IT\\5th\\Subject\\2\\IR\\P\\final projet\\IR_Project_datasets\\cacm\\files_data_set\\"       
        filesRet = files.getFiles(res,url,NLP.datasetNum) 
        return filesRet


    def query_pre_process(self, query: str):
        query = nlp.get_tokenized_list(query)
        query = nlp.remove_stopwords(query)
        q = []
        for w in nlp.word_stemmer(query):
            q.append(w)
        q = ' '.join(q)
        return q
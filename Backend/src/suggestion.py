
import joblib
from search_suggestion import SearchSuggestion


class Suggestion:

    dataset = []

    def __init__(self) -> None:
        self.load_model()
        print("start")

    def get_all_query(self,url:str):
        f = open(url, "r").read()
        query_set = []
        f = f.split('.I')
        for i in range(1, len(f)):
            # file = open(url + nameDirectoryFolder +str(i)+".txt","w+")
            f[i].replace('\n','')
            if(f[i].find('.B')!=-1):
                f[i] = f[i][f[i].index('.W')+2:f[i].index('.B')]
            else:
                f[i] = f[i][f[i].index('.W')+2:]
            f[i] = f[i].lower()
            f[i] =f[i].split()
            query_set.append(" ".join(f[i]))
        return query_set

    def get_setnt_queries(self,query_set):
        all_queries = []
        for w in range(len(query_set)):
            query = query_set[w].split(" ")
            ar = [query[0]]
            for i in range(1,len(query)):
                ar.append(ar[i-1] +" "+ query[i])
                # ar[i] = ar[i-1] + forTest[i]
            all_queries += ar
        # print(all_queries)
        return all_queries

    def merge_query_with_word_english(self,all_queries):
        words = open('D:\\IT\\5th\\Subject\\2\\IR\\P\\final projet\\project\\input\\words_english.txt', "r").read()
        words = words.split('\n')
        self.dataset = words + all_queries
        self.dataset.sort()
        return self.dataset

    def save_model(self,dataset):
        joblib.dump(dataset, "dataset_query.pkl")

    def load_model(self):
        self.dataset = joblib.load("dataset_query.pkl")
        # return dataset

    def suggest(self,word):
        ss = SearchSuggestion()
        ss.batch_insert(self.dataset)
        
        result = ss.search(word)
        print(result)
        return result





# init
q = 'D:\\IT\\5th\\Subject\\2\\IR\\P\\final projet\\IR_Project_datasets\\CISI\\CISI.QRY'
s = Suggestion()
query_set = s.get_all_query(q)
all_quer  = s.get_setnt_queries(query_set)
dataset   = s.merge_query_with_word_english(all_quer)
s.save_model(dataset)
 # Old Work






# f = open('D:\\IT\\5th\\Subject\\2\\IR\\P\\final projet\\IR_Project_datasets\\CISI\\CISI.QRY', "r").read()
# # print(f)


# query_set = []
# f = f.split('.I')
# for i in range(1, len(f)):
#         # file = open(url + nameDirectoryFolder +str(i)+".txt","w+")
#         f[i].replace('\n','')
#         if(f[i].find('.B')!=-1):
#             f[i] = f[i][f[i].index('.W')+2:f[i].index('.B')]
#         else:
#             f[i] = f[i][f[i].index('.W')+2:]
#         f[i] = f[i].lower()
#         f[i] =f[i].split()
#         query_set.append(" ".join(f[i]))


# all_queries = []
# for w in range(len(query_set)):
#     query = query_set[w].split(" ")
#     ar = [query[0]]
#     for i in range(1,len(query)):
#         ar.append(ar[i-1] +" "+ query[i])
#         # ar[i] = ar[i-1] + forTest[i]
#     all_queries += ar
# # print(all_queries)


# words = open('D:\\IT\\5th\\Subject\\2\\IR\\P\\final projet\\project\\input\\words_english.txt', "r").read()
# words = words.split('\n')





# dataset = words + all_queries
# dataset.sort()

# # joblib.dump(dataset, "dataset_query.pkl")


# from search_suggestion import SearchSuggestion

# ss = SearchSuggestion()


# # ss.insert('Waseem')

# # dataset = joblib.load("dataset_query.pkl")
# ss.batch_insert(dataset)
# joblib.dump(ss, "dataset_query.pkl")
# # result = ss.search('how')
# # print(result)





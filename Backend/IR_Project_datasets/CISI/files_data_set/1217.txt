1217
.T
Prestige, Class and Mobility
.A
Svalastoga, K.
.W
  This volume contains the report of a sample survey conducted in Denmark
1953-1954.  In addition the author has attempted to integrate survey findings
with relevant sociological theory and with previous research findings.

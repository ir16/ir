467
.T
Exploitation of Literature on Tape
.A
Rowlands, D.G.
.W
   Experience of the use of a number of commercially available magnetic tapes 
for a current awareness service is described.. Difficulties encountered in the 
assimilation of various types of tape format into the system developed for the 
Unilever Research Laboratory are discussed, and problems in the retrospective 
searching of tapes are outlined..

1370
.T
Computer Literature Searches in the Physical Sciences
.A
Murdock, L. 
Opello, O.
.W
  Selected computerized current awareness services and literature
searchs in physical sciences are listed.  The information given
includes type of literature in each data base, time period covered,
prices, and sources of availability.

1199
.T
A Method for the Construction of Minimum-Redundancy Codes
.A
Huffman, David A.
.W
   An optimum method of coding an ensemble of messages consisting of a finite 
number of members is developed.. A minimum-redundancy code is one constructed
in such a way that the average number of coding digits per message is 
minimized..

1447
.T
Information, Communication, Knowledge
.A
Ziman, J.M.
.W
  At the British Association meeting in Exeter last month,
Professor Ziman addressed the section devoted to general
topics on the question of how scientific information becomes
public knowledge.  The system of communication, he implied,
is not as rotten as some like to think.

1280
.T
Searching Natural Language Text by computer
.A
Swanson, D.R.
.W
  Machine indexing and text searching offer an approach
to the basic problems of library automation.

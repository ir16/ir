3030
.T
An Example of Hierarchical Design and Proof
.W
Hierarchical programming is being increasingly
recognized as helpful in the construction of 
large programs.  Users of hierarchical techniques claim
or predict substantial increases in productivity 
and in the reliability of the programs produced.  In this
paper we describe a formal method for hierarchical 
program specification, implementation, and proof.  We
apply this method to a significant list processing 
problem and also discuss a number of extensions to current
programming languages that ease hierarchical 
program design and proof.
.B
CACM December, 1978
.A
Spitzen, J.M.
Levitt, K.N.
Robinson, L.
.K
Program verification, specification, data abstraction,
software modules, hierarchical structures
.C
4.0 4.6 5.21 5.24
.N
CA781209 DH January 16, 19794:43 PM  

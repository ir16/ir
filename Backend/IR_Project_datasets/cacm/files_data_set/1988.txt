1988
.T
A Formalism for Translator Interactions
.W
A formalism is presented for describing the actions
of processors for programming languages-compilers,
interpreters, assemblers-and their interactions in complex systems
such as compiler-compilers or extendible languages.
The formalism here might be used to define and answer such a
question as "Can one do bootstrapping using a meta-compiler 
whose metaphase is interpretive?"  In addition an algorithm
is presented for deciding whether or not a given system can
be produced from a given set of component processors.
.B
CACM October, 1970
.A
Earley, J.
.K
translator, compiler, interpreter, bootstrapping,
language processor, compiler-compiler
.C
4.1 5.29
.N
CA701002 JB February 10, 1978  9:36 AM

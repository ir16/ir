1927
.T
Information Science in a Ph.D. Computer Science Program
.W
This report contains recommendations on a sample course
curriculum in the general area of information 
organization and information system design in a Ph.D.
Computer Science Program.  The subject area is 
first briefly described, followed by a listing of some desirable
graduate-level courses.  Suitable bibliographies 
are appended.
.B
CACM February, 1969
.A
Salton, G.
.K
course curriculum, graduate courses, university
courses,computer science curriculum, information 
science, information organization, information retrieval,
data retrieval, language analysis, information 
processing
.C
1.52 3.42 3.70
.N
CA690211 JB February 20, 1978  9:50 AM

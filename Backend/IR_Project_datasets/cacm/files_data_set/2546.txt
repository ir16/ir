2546
.T
The Use of Grammatical Inference for Designing Programming Languages
.W
Both in designing a new programming language
and in extending an existing language, the designer 
is faced with the problem of deriving a "natural" grammar
for the language.  We are proposing an interactive 
approach to the grammar design problem wherein the designer
presents a sample of sentences and structures 
as input to a grammatical inference algorithm.  The algorithm
then constructs a grammar which is a reasonable 
generalization of the examples submitted by the designer.
 The implementation is presently restricted 
to a subclass of operator precedence grammars, but
a second algorithm is outlined which applies to a 
larger class of context-free grammars.
.B
CACM February, 1973
.A
Crespi-Reghizzi, S.
Melkanoff, M. A.
Lichten, L.
.K
grammar design, language definition, inference,
identification in the limit, extensible languages
.C
3.61 4.2 5.23
.N
CA730202 JB January 24, 1978  3:20 PM

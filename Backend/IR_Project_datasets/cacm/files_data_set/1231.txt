1231
.T
Peephole Optimization
.W
Redundant instructions may be discarded during
the final stage of compilation by using a simple 
optimizing technique called peephole optimization.
The method is described and examplesare given.
.B
CACM July, 1965
.A
McKeeman, W. M.
.N
CA650704 JB March 6, 1978  9:05 PM

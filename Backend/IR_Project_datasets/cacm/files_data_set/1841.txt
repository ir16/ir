1841
.T
A Prime Number Generator Using The
Treesort Principle (Algorithm 356 [A1])
.B
CACM October, 1969
.A
Singleton, R. C.
.K
prime numbers, number theory, sorting
.C
3.15 5.30 5.31
.N
CA691005 JB February 15, 1978  3:25 PM

1987
.T
Student's t-Distribution $S14) (Algorithm 395)
.B
CACM October, 1970
.A
Hill, G. W.
.K
Student's t-statistic, distribution function,
approximation, asymptotic expansion
.C
5.12 5.5
.N
CA701003 JB February 10, 1978  9:31 AM

2844
.T
Heaps Applied to Event Driven Mechanisms
.B
CACM July, 1976
.A
Gonnet, G. H.
.K
discrete event simulation, event-scanning
mechanisms, priority queues, heaps
.C
4.34 8.1
.N
CA760709 JB January 4, 1978  11:05 AM

2041
.T
Shellsort (Algorithm 201 $M1))
.B
CACM June, 1970
.A
Chandler, J. P.
Harrison, W. C.
.K
sorting, minimal storage sorting, digital computer sorting
.C
5.31
.N
CA700614 JB February 13, 1978  10:29 AM

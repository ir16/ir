2132
.T
Rapid Computation of Coefficients of Interpolation
Formulas [E1] (Algorithm 416)
.B
CACM December, 1971
.A
Gustafson, S.
.K
divided differences, Newton's interpolation formula
.C
5.13
.N
CA711211 JB February 2, 1978  9:36 AM

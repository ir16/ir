2530
.T
An Algorithm for Extracting Phrases in
a Space-Optimal Fashion [Z] (Algorithm A444)
.B
CACM March, 1973
.A
Wagner, R. A.
.K
information retrieval, coding, text compression
.C
3.70 5.6
.N
CA730309 JB January 24, 1978  10:26 AM

2021
.T
A Comment on Axiomatic Approaches to Programming
.B
CACM July, 1970
.A
Hunt, B.R.
.K
axiomatic method, proofs of programs,
homomorphic structure in programming
.C
4.0 4.21 4.22 5.20 5.21 5.23 5.24
.N
CA700716 JB February 10, 1978  4:15 PM

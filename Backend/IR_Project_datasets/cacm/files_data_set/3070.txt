3070
.T
Hybrid Simulation Models of Computer Systems
.W
This paper describes the structure and operation
of a hybrid simulation model in which both 
discrete-event simulation and analytic techniques are
combined to produce efficient yet accurate system 
models.  In an example based on a simple hypothetical
computer system, discrete-event simulation is used 
to model the arrival and activation of jobs, and a
central-server queueing network models the use of 
system processors.  The accuracy and efficiency of the
hybrid technique are demonstrated by comparing 
the result and computational costs of the hybrid model of
the example with those of an equivalent simulation-only 
model.  
.B
CACM September, 1978
.A
Schwetman, H.D.
.K
Performance evaluation, simulation, queueing
network models, central server model
.C
4.32 4.35 8.1
.N
CA780902 DH February 5, 1979  3:32 PM

3113
.T
Optimal Conversion of Extended-Entry
Decision Tables with General Cost Criteria
.W
A general dynamic programming algorithm for converting
limited, extended, or mixed entry decision 
tables to optimal decision trees is presented which can
take into account rule frequencies or probabilities, 
minimum time and/or space cost criteria, common action
sets, compressed rules and ELSE rules, sequencing 
constraints on condition tests, excludable combinations
of conditions, certain ambiguities, and interrupted 
rule masking. 
.B
CACM April, 1978
.A
Lew, A.
.K
Decision table, optimal programs, dynamic programming
.C
3.59 4.19 4.29 4.49 5.39 5.42 8.3
.N
CA780403 DH February 26, 1979  4:37 PM

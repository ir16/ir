1723
.T
Computer Construction of Project Networks
.W
Project networks are used in PERT and CPM.
 An algorithm is given for constructing project 
networks directly from the project precedence relations.
 The algorithm creates "dummy" activities and 
topologically orders the arcs and nodes.  The number of
nodes created is minimal for the given precedence 
relations.  It has been experimentally programmed
in FORTRAN II for the IBM 7094.
.B
CACM July, 1968
.A
Fisher, A. C.
Liebman, J. S.
Nemhauser, G. L.
.K
project networks, PERT, CPM, topological
ordering, network construction by computer
.C
5.32
.N
CA680706 JB February 22, 1978  12:07 PM

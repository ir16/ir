1533
.T
A Marovian Model of the University of Michigan Executive System
.W
A mathematical model of a computer's executive
system is postulated and its parameters estimated 
with the aid of extensive data on the system's operation.
 Although simplifying assumptions are made, 
the results predicted by the model agree reasonable well
with actual results.  The model is used to study 
the effects of changes in the executive system and
in one of its compilers.  Further applications of 
the model are discussed.
.B
CACM September, 1967
.A
Foley, J. D.
.N
CA670911 JB February 27, 1978  2:50 PM

747
.T
Generation of Permutations in Lexicographical Order (Algorithm 202)
.B
CACM September, 1963
.A
Shen, M. K.
.N
CA630911 JB March 13, 1978  7:31 PM

1551
.T
On Compiling Algorithms for Arithmetic Expressions
.W
This paper deals with algorithms concerning arithmetic
expressions used in a FORTRAN IV compiler 
for a HITAC-5020 computer having n accumulators.  The
algorithms generate an object code which minimizes 
the frequency of storing and recovering the partial results
of the arithmetic expressions in cases where 
there are several accumulators.
.B
CACM August, 1967
.A
Nakata, I.
.N
CA670804 JB February 27, 1978  4:35 PM

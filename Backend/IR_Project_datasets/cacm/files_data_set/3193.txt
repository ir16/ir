3193
.T
.W
   Work is in progress on a formula coding technique allowing direct entry
into the computer of formulae typed on an 84 character Flexo-writer. This
Flexo-writer will be modified for automatic half-line advance and retract,
without carriage return, to permit completely general sub and superscripting.
.B
CACM July, 1958
.N
CA580703 ES March 17, 1982 10:10 AM

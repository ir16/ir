203
.T
Decoding Combinations of the First n Integers Taken k at a Time
.B
CACM April, 1960
.A
Brown, R. M.
.N
CA600408 JB March 22, 1978  1:46 PM

860
.T
A Comparison Between the Polyphase and Oscillating Sort Techniques
.W
A comparison between the Oscillating and Polyphase
Sort techniques is developed for computer 
systems having from four to ten tape drives.  The basis
for the comparison is the total reading and writing 
required for various number of input strings
and tape drives for the two techniques.
.B
CACM May, 1963
.A
Goetz, M. A.
Toth, G. S.
.N
CA630507 JB March 14, 1978  11:25 AM

1005
.T
Coordinates on an Ellipsoid (Algorithm 240 [Z])
.B
CACM September, 1964
.A
Dorrer, E.
.N
CA640906 JB March 9, 1978  6:09 PM

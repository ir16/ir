2224
.T
Complex Gamma Function [S14] (Algorithm 404)
.B
CACM January, 1971
.A
Lucas Jr., C. W.
Terrill, C. W.
.K
gamma function, poles of gamma function, Stirling's
asymptotic series, recursion formula, reflection 
formula
.C
5.12
.N
CA710110 JB February 8, 1978  10:25 AM

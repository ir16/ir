1712
.T
Recovery of Disk Contents After System Failure
.W
A method is discussed by which, after a system
malfunction, the contents of disk files can 
be restored to their status at the time of the failure.
.B
CACM August, 1968
.A
Lockemann, P. C.
Knutsen, W. D.
.K
data acquisitition, disk file organization,
error recovery, file organization
.C
3.73
.N
CA680802 JB February 22, 1978  11:14 AM

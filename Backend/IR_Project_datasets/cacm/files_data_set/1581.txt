1581
.T
Complementary Error Function-Large X (Algorithm 181 [S15])
.B
CACM June, 1967
.A
Hill, I. D.
Joyce, S. A.
.N
CA670607i JB March 23, 1978  3:37 PM

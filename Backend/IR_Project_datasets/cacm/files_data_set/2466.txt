2466
.T
Enumerating Combinations of m Out of n Objects [G6] (Algorithm A452)
.B
CACM August, 1973
.A
Liu, C. N
Tang, D. T.
.K
permutations, combination
.C
5.30
.N
CA730808 JB January 23, 1978  10:39 AM

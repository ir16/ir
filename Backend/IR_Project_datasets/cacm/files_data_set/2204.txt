2204
.T
Program Development by Stepwise Refinement
.W
The creative activity of programming-to be distinguished
from coding-is usually taught by examples 
serving to exhibit certain techniques.  It is here considered
as a sequence of design decisions concerning 
the decomposition of tasks into subtasks and of data
into data structures.  The process of successive 
refinement of specifications is illustrated by a short
but nontrivial example, from which a number of 
conclusions are drawn regarding the art and the instruction of programming.
.B
CACM April, 1971
.A
Wirth, N.
.K
education in programming, programming
techniques, stepwise program construction
.C
1.50 4.0
.N
CA710401 JB February 3, 1978  4:14 PM

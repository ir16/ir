2641
.T
A Minimal Spanning Tree clustering Method [Z] (Algorithm A479)
.B
CACM June, 1974
.A
Page, R. L.
.K
clustering, pattern recognition, feature
selection, minimal spanning trees
.C
3.63 5.39 5.5
.N
CA740605 JB January 17, 1978  3:06 PM

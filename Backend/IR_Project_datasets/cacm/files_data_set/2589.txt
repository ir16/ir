2589
.T
A Computer Routine for Quadratic and Linear
Programming Problems (Algorithm R431)
.B
CACM October, 1974
.A
Proll, L. G.
.N
CA741009 JB January 16, 1978  11:10 AM

1367
.T
Character Structure and Character Parity Sense
for Parallel-by-Bit Data Communication in ASCII* 
(Proposed American Standard)
.B
CACM September, 1966
.N
CA660912 JB March 2, 1978  4:26 PM

2350
.T
Clenshaw-Curtis Quadrature [D1] (Algorithm A424)
.B
CACM May, 1972
.A
Gentleman, W.M.
.K
quadrature, Chebyshev series, cosine transform, fast Fourier transform
.C
5.1
.N
CA720510 JB January 31, 1978  9:37 AM 

2874
.T
A Comparative Evaluation of Versions of BASIC
.W
From its inception, The BASIC language has
grown in terms of its usage, scope of usage, and 
its features.  This article compares ten of the current
versions of BASIC with each other, with two earlier 
versions, and with the proposed standard for minimal
BASIC. The comparison is arranged by the features 
of the versions and by computational comparison
of computation and times and processing costs.
.B
CACM April, 1976
.A
Lientz, B. P.
.K
BASIC, interpretive language summary
.C
4.20 4.6
.N
CA760402 JB January 4, 1978  4:34 PM

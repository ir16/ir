2052
.T
Scheduling to Reduce Conflict in Meetings
.W
Conflicts in scheduling can be treated as defining an undirected linear graph 
independently of the relation of the activities in conflict to additional 
constraints of time and space.  Each connected component of such a graph,
which can be found by an algorithm described by Gotlieb and Corneil, 
corresponds to a set of events that must be scheduled at different times.
.B
CACM June, 1970
.A
Grimes, J. E.
.K
allocation, conflict matrix, connected component, scheduling, spanning
tree, undirected linear graph
.C
1.90 3.51 3.59 5.32
.N
CA700603 JB February 13, 1978  11:55 AM

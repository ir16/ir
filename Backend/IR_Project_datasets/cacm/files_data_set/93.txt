93
.T
From Formulas to Computer Oriented Language
.W
A technique is shown for enabling a computer
to translate simple algebraic formulas into a 
three address computer code.
.B
CACM March, 1959
.A
Wegstein, J. H.
.N
CA590303 JB March 22, 1978  8:03 PM

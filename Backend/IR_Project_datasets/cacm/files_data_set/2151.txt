2151
.T
User Program Measurement in a Time-Shared Environment
.W
A general discussion of the measurement of
software systems is followed by a description of 
a hardware and software scheme for measuring user programs
in a time-shared environment.  The TX-2 computer 
at MIT Lincoln Laboratory was used for the implementation
of such a system and the characteristics of 
this implementation are reported.  A scenario showing
the system in use is presented.  Finally, it is 
shown how other time-sharing systems may provide similar measuring facilities.
.B
CACM October, 1971
.A
Nemeth, A. G.
Rovner, P. D.
.K
operating systems, multiprogramming systems, time-sharing
systems, software measurement, user program 
measurement, measurement technology, TX-2 computer,
virtual computers, performance improvement
.C
4.30 4.32 4.42 4.43
.N
CA711005 JB February 2, 1978  1:05 PM

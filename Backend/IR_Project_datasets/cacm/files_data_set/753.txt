753
.T
A Procedure for Converting Logic Table Conditions
into an Efficient Sequence of Test Instructions
.B
CACM September, 1963
.A
Egler, J. F.
.N
CA630905 JB March 13, 1978  7:40 PM

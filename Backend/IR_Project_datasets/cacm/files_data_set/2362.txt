2362
.T
Linear Equation Solver [F4] (Algorithm A423)
.B
CACM April, 1972
.A
Moler, C. B.
.K
matrix algorithms, linear equations, Fortran,
paged memory, virtual memory, array processing
.C
4.22 4.32 5.14
.N
CA720411 JB January 31, 1978  12:34 PM

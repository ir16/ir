2498
.T
Minimizing Wasted Space in Partitioned Segmentation
.W
A paged virtual memory system using a finite
number of page sizes is considered.  Two algorithms 
for assigning pages to segments are discussed.  Both
of these algorithm are simple to implement.  The 
problem of choosing the page sizes to minimize the expected
value of total wasted space in internal fragmentation 
and in a page table, per segment, is then solved for a
probability density function of segment size which 
may be expressed as a convex combination of Erlang densities.
.B
CACM June, 1973
.A
Gelenbe, E.
.K
dynamic storage allocation, virtual memory, paging,
multiple page sizes, fragmentation, segmentation
.C
4.0 4.3 4.32 6.34 8.3
.N
CA730602 JB January 23, 1978  3:38 PM

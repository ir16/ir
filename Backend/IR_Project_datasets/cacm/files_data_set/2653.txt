2653
.T
Solution of the Transcendental Equation w*exp(x)=x (Algorithm R443)
.B
CACM April, 1974
.A
Einarsson, B.
.N
CA740417 JB January 17, 1978  4:44 PM

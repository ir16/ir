2673
.T
Quadratic Search for Hash Tables of Size p^n
.B
CACM March, 1974
.A
Ackerman, A. F.
.K
hashing, quadratic search
.C
4.10
.N
CA740310 JB January 18, 1978  10:58 AM

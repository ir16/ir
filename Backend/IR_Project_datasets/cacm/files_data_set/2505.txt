2505
.T
Reflection-Free Permutations, Rosary Permutations,
and Adjacent Transposition Algorithms
.B
CACM May, 1973
.A
Roy, M. K.
.K
permutation, permutation generation, scheduling, combinatorial analysis
.C
5.39
.N
CA730515 JB January 23, 1978  4:29 PM

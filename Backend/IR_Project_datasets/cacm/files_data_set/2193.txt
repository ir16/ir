2193
.T
On Implementation of Label Variables
.W
Variables of label mode are conventionally
implemented with a technique which fails to trap 
certain programming errors.  Fine-grained calendar clocks
have recently become available; these allow 
implementation of label variables via a new technique
which traps all programming errors of this variety.
.B
CACM May, 1971
.A
Fenichel, R. R.
.K
labels, compiler, interpreter, go to, transfer
.C
4.22
.N
CA710506 JB February 3, 1978  2:31 PM

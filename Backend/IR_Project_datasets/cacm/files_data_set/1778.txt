1778
.T
F-Distribution (Algorithm 322 [S14])
.B
CACM February, 1968
.A
Dorrer, E.
.K
Fisher's  F-distribution, Student's t-distribution
.C
5.5
.N
CA680204 JB February 23, 1978  12:06 PM

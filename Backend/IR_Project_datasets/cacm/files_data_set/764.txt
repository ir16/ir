764
.T
Reduction of a Matrix Containing Polynomial Elements (Algorithm 170)
.B
CACM August, 1963
.A
Hennion, P. E.
.N
CA630834 JB March 13, 1978  8:09 PM

485
.T
Real Error Function, ERF(x) (Algorithm 123)
.B
CACM September, 1962
.A
Crawford, M.
Techo, R.
.N
CA620907 JB March 17, 1978  7:35 PM

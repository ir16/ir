1044
.T
An Automatic Loader for Subroutine Nests
.W
A method for automatic loading of library subroutines,
which can be adapted to operate in conjunction 
with any conventional two-pass assembler is described.
 The method is specifically designed to cope with 
a nested library structure.
.B
CACM July, 1964
.A
Kanner, H.
.N
CA640703 JB March 9, 1978  8:27 PM

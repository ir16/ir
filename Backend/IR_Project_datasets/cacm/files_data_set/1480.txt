1480
.T
Computation of Chebyshev Series Coefficients (Algorithm 277[C6])
.B
CACM February, 1966
.A
Smith, L. B.
.N
CA660206c JB March 23, 1978  7:28 PM

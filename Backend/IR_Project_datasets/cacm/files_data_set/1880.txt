1880
.T
Chebyshev Solution to an Overdetermined
Linear System (Algorithm 328 [F4])
.B
CACM June, 1969
.A
Golub, G. H.
.K
Chebyshev solutions, over-determined linear
systems, linear equations, exchange algorithm 
.C
5.13 5.14 5.41
.N
CA690613 JB February 17, 1978  9:49 AM

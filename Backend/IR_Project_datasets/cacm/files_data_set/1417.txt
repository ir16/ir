1417
.T
Economies of Scale and the IBM System/360
.W
Cost functions among five System/360 models
are analyzed through examinations of instruction 
times, program kernels and a "typical" instruction mix.
 Comparisons are made between the data developed 
here and Grosch's Law which seems to be applicable to
much of the data.  Sizable economies of scale are 
unquestionably present in computing equipment. 
.B
CACM June, 1966
.A
Solomon Jr., M. B.
.N
CA660604 JB March 3, 1978  8:57 AM

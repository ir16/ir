1683
.T
Boolean matrix Methods for the Detection of Simple Precedence Grammars
.W
A mechanical procedure is derived for determining
whether a given context-free phrase structure 
grammar is a simple precedence grammar.  This procedure
consists of elementary operations on suitably 
defined Boolean matrices.  Application of the
procedure to operator grammars is also given.
.B
CACM October, 1968
.A
Martin, D. F.
.K
syntax analysis, precedence analysis, simple precedence
grammar, simple precedence language, operator 
grammar, operator precedence, compilers, bounded-context
syntactic analysis, Boolean matrices, relations
.C
4.12 5.23
.N
CA681004 JB February 21, 1978  4:09 PM

2443
.T
Generation of Permutations in Lexicographic Order (Algorithm R323)
.B
CACM September, 1973
.A
Roy, M. K.
.N
CA730912 JB January 23, 1978  8:39 AM

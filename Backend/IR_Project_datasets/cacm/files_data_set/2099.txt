2099
.T
Eigenvalues and Eigen vectors of a
Real General Matrix (Algorithm 343 $F))
.B
CACM February, 1970
.A
Knoble, H. D.
.K
norm, characteristic equation, degenerate eigen-system,
diagonalizable matrix, defective matrix  
.C
5.14
.N
CA700216 JB February 14, 1978  9:43 AM

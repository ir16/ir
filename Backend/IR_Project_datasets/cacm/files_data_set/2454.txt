2454
.T
Computational Algorithms for Closed Queueing
Networks with Exponential Servers
.W
Methods are presented for computing the equilibrium
distribution of customers in closed queueing 
networks with exponential servers.  Expressions for
various marginal distributions are also derived. 
 The computational algorithms are based on two-dimensional
iterative techniques which are highly efficient 
and quite simple to implement.  Implementation considerations
such as storage allocation strategies and 
order of evaluation are examined in some detail.
.B
CACM September, 1973
.A
Buzen, J. P.
.K
queueing theory, queueing networks, equilibrium
distributions, steady state distributions
.C
5.12 5.5 8.1 8.3
.N
CA730901 JB January 23, 1978  10:07 AM

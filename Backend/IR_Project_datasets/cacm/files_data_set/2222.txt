2222
.T
Comment on London's Certification of Algorithm 245
.B
CACM January, 1971
.A
Redish, K. A.
.K
proof of algorithms, debugging, certification,
metatheory,  sorting, in-place sorting
.C
4.42 4.49 5.24 5.31
.N
CA710112 JB February 8, 1978  10:19 AM

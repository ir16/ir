1460
.T
Evolution of the Meta-Assembly Program
.W
A generalized assembler called a "meta-assembler"
is described.  The meta-assembler is defined 
and factors which contributed to its evolution are presented.
 How a meta-assembler is made to function 
as an assembly program is described. Finally, the implication
of meta-assemblers on compiler design is 
discussed.
.B
CACM March, 1966
.A
Ferguson, D. E.
.N
CA660312 JB March 3, 1978  11:56 AM

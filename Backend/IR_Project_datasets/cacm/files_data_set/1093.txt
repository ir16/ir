1093
.T
Evaluation of Determinant (Algorithm 224)
.B
CACM April, 1964
.A
Rotenberg, L. J.
.N
CA640415 JB March 10, 1978  1:12 AM

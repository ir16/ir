2696
.T
A Method of Bivariate Interpolation and Smooth
Surface Fitting Based on Local Procedures
.W
A method is designed for interpolating values
given at points of a rectangular grid in a plane 
by a smooth bivariate function z=z(x,Y).  The interpolating
function is a bicubic polynomial in each 
cell of the rectangular grid.  Emphasis is an avoiding
excessive undulation between given grid points. 
The proposed method is an extension of the method of
univariate interpolation developed earlier by the 
author and is likewise based on local procedures.
.B
CACM January, 1974
.A
Akima, H.
.K
bivariate interpolation, interpolation, partial
derivative, polynomial, smooth surface fitting
.C
5.13
.N
CA740103 JB January 18, 1978  2:38 PM

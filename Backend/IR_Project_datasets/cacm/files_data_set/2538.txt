2538
.T
A Computer Science Course Program for Small Colleges
.W
The ACM Subcommittee on Small College Programs
of the Committee on Curriculum in Computer Science 
(CCCS) was appointed in 1969 to consider the unique
problems of small colleges and universities, and 
to make recommendations regarding computer science programs
at such schools.  This report, authorized 
by both the subcommittee and (CCCS), supplies a set of
recommendations for courses and necessary resources. 
 Implementation problems are discussed, specifically
within the constraints of limited faculty and for 
the purposes of satisfying a wide variety of objectives.
 Detailed description of four courses are given; 
suggestions are made for more advanced work;
and an extensive library list is included.
.B
CACM March, 1973
.A
Austing, R. H.
Engel, G. L.
.K
computer science education, course proposals, small
colleges, programming course, social implications 
course, computer organization course, file organization course, bibliographies
.C
1.52
.N
CA730301 JB January 24, 1978  1:22 PM

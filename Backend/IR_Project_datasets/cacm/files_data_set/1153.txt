1153
.T
A Fast Procedure for Generating Normal Random Variables*
.W
A technique for generating normally distributed
random numbers is described.  It is faster 
than those currently in general use and is readily
applicable to both binary and decimal computers.
.B
CACM January, 1964
.A
Marsaglia, G.
MacLaren, M. D.
Bray, T. A.
.N
CA640101 JB March 10, 1978  5:43 AM

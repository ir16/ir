1960
.T
Process Management and Resource Sharing in the Multiaccess System ESOPE
.W
The main design principles of the multiaccess system ESOPE are described.
Emphasis is placed on basic ideas underlying the design rather
than on implementation details.  The main features of the system
include the ability given to any user to schedule his own parallel
processes using system primitive operations, the file-memory relationship,
and the allocation-scheduling policy, which dynamically
takes into account recent information about user behavior.
.B
CACM December, 1970
.A
Betourne, C.
Boulenger, J.
Ferrie, J.
Kaiser, C.
Krakowiak, S.
Mossiere, J.
.K
time-sharing, multiprogramming, process scheduling, resource allocation
.C
4.32
.N
CA701203 JB February 9, 1978  4:02 PM

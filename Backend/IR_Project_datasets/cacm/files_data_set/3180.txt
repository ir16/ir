3180
.T
Costs of the Current U.S. Payments System
.W
Neither the banking industry nor
public policy makers have good information on the comparative costs
of alternative payment systems such as cash, checks, credit cards,
and EFT transactions.  As a result, EFT systems and services are
likely to be implemented without a valid assessment of whether they
are cost-justified, lst alone justified in terms of other criteria.
.B
CACM December, 1979
.A
Lipis, A.H.
.K
EFT's,payment system costs, payment system volumes
.C
3.52
.N
CA791203 DB February 25, 1980  11:09 AM

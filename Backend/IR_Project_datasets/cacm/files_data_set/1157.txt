1157
.T
Procedure for the Normal Distribution Functions (Algorithm 272 [S15])
.B
CACM December, 1965
.A
MacLaren, M. D.
.N
CA651212 JB March 6, 1978  3:37 PM

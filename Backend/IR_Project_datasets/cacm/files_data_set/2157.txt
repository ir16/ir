2157
.T
Average Binary Search Length for Dense Ordered Lists
.B
CACM September, 1971
.A
Flores, I.
Madpis, G.
.K
searching, binary searching, record retrieval
.C
3.74
.N
CA710907 JB February 2, 1978  1:42 PM

1787
.T
Use of Transition Matrices in Compiling
.W
An algorithms is described which constructs
from a suitable BNF grammar an efficient left-right 
recognizer for sentences of the corresponding language.
 The type of recognizer, used in a number of 
compilers, operates with a pushdown stack and with
a transition matrix.  Two examples illustrate how 
such recognizers may be used effectively for other
purposes besides the usual syntax checking.
.B
CACM January, 1968
.A
Gries, D.
.K
transition matrices, compilation, translation,
grammar, context-free language, formal language, 
parsing
.C
4.12 5.23
.N
CA680107 JB February 23, 1978  2:35 PM

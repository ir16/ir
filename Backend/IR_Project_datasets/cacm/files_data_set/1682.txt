1682
.T
The Implementation of a BASIC System in a Multiprogramming Environment
.W
The implementation of a remote terminal BASIC system
within the context of an existing multiprogramming 
computer system, the Burroughs B5500, is described.
 This implementation combines a unique mixture of 
machine language and interpretive techniques with an incremental compiler.
.B
CACM October, 1968
.A
Braden, H. V.
Wulf, W. A.
.K
multiprogramming, incremental compilation, compilers, interpreters
.C
4.1 4.12 4.2 4.22 4.3 4.32
.N
CA681005 JB February 21, 1978  4:04 PM

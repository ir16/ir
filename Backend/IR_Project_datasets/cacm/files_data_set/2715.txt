2715
.T
Implementation of a Structured English Query Language
.W
The relational model of data, the XRM Relational
Memory System, and the SEQUEL language have 
been covered in previous papers and are reviewed. 
SEQUEL is a relational data sublanguages intended 
for the ad hoc interactive problem solving by non-computer
specialists.  A version of SEQUEL that has 
been implemented in a prototype interpreter is described.
 The interpreter is designed to minimize the 
data accessing operations required to respond to an arbitrary
query.  The optimization algorithms designed 
for this purpose are described.
.B
CACM October, 1975
.A
Astrahan, M. M.
Chamberlin,D. D.
.K
relational model, query language, nonprocedural language,
database, data structure, data organization
.C
3.74 3.75 4.22 4.33 4.34
.N
CA751004 JB January 6, 1978  11:02 AM

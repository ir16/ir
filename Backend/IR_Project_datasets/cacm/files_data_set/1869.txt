1869
.T
Block Structures, Indirect Addressing, and Garbage Collection
.W
Programming languages have included explicit
or implicit block structures to provide a naming 
convenience for the programmer.  However, when indirect
addressing is used, as in SNOBOL, naming constraints 
may be introduced.  Two modifications to SNOBOL are described,
resulting in two desirable consequences: 
(1) naming constraints disappear even when there is
indirect addressing within function definitions; 
and (2) there is a significant saving in the number of
calls to the garbage collector, because some garbage 
is collected, at little expense, each time a function
returns to its calling program.  These modifications 
have been implemented as an extension to a SNOBOL dialect.
.B
CACM July, 1969
.A
Kain, R. Y.
.K
block structures, indirect addressing,
garbage collection, local names, SNOBOL
.C
4.22
.N
CA690711 JB February 15, 1978  6:49 PM

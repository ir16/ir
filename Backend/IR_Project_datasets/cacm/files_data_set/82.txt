82
.T
Handling Identifiers as Internal Symbols in Language Processors
.W
Substitution of computer-oriented symbols for
programmer-oriented symbols in language processors 
is examined and a feasible method for doing so is presented.
.B
CACM June, 1959
.A
Williams, F. A.
.N
CA590602 JB March 22, 1978  6:38 PM

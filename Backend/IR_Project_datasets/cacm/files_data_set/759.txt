759
.T
Continued Operation Notation for Symbol
Manipulation and Array Processing
.W
A brief account is given of a notational device
that is very useful in the formal representation 
of syntaxes, string relationships and string transformation
procedures and also of computing procedures 
that deal with arrays of functions of many variables. 
The device consists of the use of certain "continued 
operation" or "collective" symbols that are analogous to the summation
symbol (Sigma) and continued multiplication 
symbol (Pi) of conventional mathematics.
.B
CACM August, 1963
.A
Barnett, M. P.
.N
CA630839 JB March 13, 1978  8:02 PM

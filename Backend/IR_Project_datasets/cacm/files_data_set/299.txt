299
.T
A Generalized Polyphase Merge Algorithm
.B
CACM August, 1961
.A
Reynolds, S. W.
.N
CA610805 JB March 16, 1978  10:10 PM

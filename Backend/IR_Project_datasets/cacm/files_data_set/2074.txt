2074
.T
Sqank (Algorithm 379 $D1))
.B
CACM April, 1970
.A
Lyness, J. N.
.K
numerical integration, integration rule, adaptive integration,
automatic integration, Simpson's rule, numerical quadrature, quadrature, 
quadrature rule, adaptive quadrature, 
automatic quadrature, round-off error control
.C
5.16
.N
CA700410 JB February 13, 1978  2:46 PM

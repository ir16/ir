2691
.T
Comments on the Algorithms of Verhelst for the
Conversion of Limited-Entry Decision Tables to 
Flowcharts
.B
CACM January, 1974
.A
King, P. J. H.
Johnson, R. G.
.K
decision table, flowcharting, preprocessor, optimal programs, search 
.C
3.50 3.59 4.19 4.29 4.49 5.31
.N
CA740108 JB January 18, 1978  1:45 PM

This project is an information retrieval system
Dataset is taken from:
	1- https://www.kaggle.com/datasets/dmaso01dsta/cisi-a-dataset-for-information-retrieval
	2- http://ir.dcs.gla.ac.uk/resources/test_collections/cacm/

Backend
It was developed using python

IR-Backend structural:
VSM : vector space model --> for match index with query
NLP : for pre-processing

IR-Frontend
It was developed using the framework angular